import React, { useEffect } from 'react';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Login from './pages/app/login';
import MainRoutes from './router/routes';
import Register from './pages/app/register';

function App() {
  const token = localStorage.getItem('token')

  // jika blm login maka cmn bisa buka login / register
  if(token === null) {
    return <div>
      <BrowserRouter>
        <Switch>
          <Route path="/login">
              <Login />
          </Route>
          <Route path="/register">
              <Register />
          </Route>
          <Route path="/">
              <Login />
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  }

  // jika udah login maka bisa buka semaunya
  if(token) {
    return <div>
      {/* <Dashboard /> */}
      <BrowserRouter>
        <Switch>
          <MainRoutes />
        </Switch>
      </BrowserRouter>
    </div>
  }


  return (
    <div className="wrapper">
      <h1>Application</h1>
      <BrowserRouter>
        <Switch>
          <MainRoutes />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;