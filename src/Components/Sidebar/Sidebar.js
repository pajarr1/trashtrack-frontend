import React, { Component, useEffect, useState } from "react";
import { useLocation, NavLink, useHistory } from "react-router-dom";

import { Nav } from "react-bootstrap";
import profile from "../../assets/img/profile.png";
import logo from "../../assets/img/logo-normal.svg";
import settings from "../../assets/img/settings.svg";
import logout from "../../assets/img/logout.svg";
import axios from "axios";

function Sidebar({ color, image, routes, petugasRoute }) {
  const location = useLocation();
  const history = useHistory();
  const [username, setUsername] = useState();
  const [role, setRole] = useState();
  const activeRoute = (routeName) => {
    return location.pathname.indexOf(routeName) > -1 ? "active" : "";
  };

  useEffect(() => {
    setUsername(localStorage.getItem("username"));
    axios
      .get("http://localhost:3333/api/v1/checkrole", {
        params: {
          user_id: localStorage.getItem("user_id"),
          username: localStorage.getItem("username"),
        },
      })
      .then((res) => {
        setRole(res.data.role);
      });
  }, 1000);

  const Logout = (e) => {
    localStorage.removeItem("token");
    const checkToken = localStorage.getItem("token");
    if (!checkToken) {
      history.go("/login");
      e.preventDefault();
    }
  };

  function UserGreeting() {
    if(role === 'admin') {
      return <div>
      {routes.map((prop, key) => {
        if (!prop.redirect)
          return (
            <li
              className={
                prop.upgrade ? "active active-pro" : activeRoute(prop.path)
              }
              key={key}
            >
              <div
                onClick={(e) => {
                  history.push(prop.path);
                  window.location.reload();
                }}
                className="nav-link"
              >
                <i className={prop.icon} />
                <p>{prop.name}</p>
              </div>
            </li>
          );
        return null;
      })}

      </div>
    } else if(role === 'petugas') {
      return <div>
      {petugasRoute.map((prop, key) => {
        if (!prop.redirect)
          return (
            <li
              className={
                prop.upgrade ? "active active-pro" : activeRoute(prop.path)
              }
              key={key}
            >
              <div
                onClick={(e) => {
                  history.push(prop.path);
                  window.location.reload();
                }}
                className="nav-link"
              >
                <i className={prop.icon} />
                <p>{prop.name}</p>
              </div>
            </li>
          );
        return null;
      })}

      </div>
    }

  }


  return (
    <div className="sidebar bg-primary" data-image={image} data-color={color}>
      <div
        className="sidebar-background"
        style={{
          backgroundColor: "",
        }}
      />
      <div className="sidebar-wrapper">
        <div className=" d-flex align-items-center justify-content-center">
          <div className="mt-4">
            <img src={logo} alt="..." className="logo-white" width="90" />
          </div>
        </div>
        <Nav>
          <UserGreeting />
        </Nav>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <div className=" d-flex align-item-center justify-content-center mt-5 pt-5">
          <img src={profile} style={{ maxWidth: "150px" }} />
        </div>
        <center className="mt-3">
          <h6 className="fw-bold">{ username } - {role}</h6>
        </center>
        <div className=" d-flex align-item-center justify-content-center mt-3">
          <img src={settings} style={{ maxWidth: "150px" }} className="mx-2" />
          <img
            src={logout}
            style={{ maxWidth: "150px" }}
            className="mx-2"
            onClick={Logout}
          />
        </div>
      </div>
    </div>
  );
}

export default Sidebar;
