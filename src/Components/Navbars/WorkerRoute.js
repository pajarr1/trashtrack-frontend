/*!

=========================================================
* Light Bootstrap Dashboard React - v2.0.1
=========================================================

* Product Page: https://www.creative-tim.com/product/light-bootstrap-dashboard-react
* Copyright 2022 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/light-bootstrap-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Dashboard from "../../pages/app/Dashboard";
import DataUser from "../../pages/admin/DataUser";
import DataAdmin from "../../pages/admin/DataSampah";
import DashboardWorker from "../../pages/worker/DashboardWorker";
// import UserProfile from "views/UserProfile.js";
// import TableList from "views/TableList.js";
// import Typography from "views/Typography.js";
// import Icons from "views/Icons.js";
// import Maps from "views/Maps.js";
// import Notifications from "views/Notifications.js";
// import Upgrade from "views/Upgrade.js";



// dashboard
// data user
// data sampah
// data pengangkut

const WorkerRoute = [
  {
    path: "/dashboard-worker",
    name: "Dashboard",
    icon: "nc-icon nc-chart-pie-35",
    component: DashboardWorker,
  },
  {
    path: "/assignment",
    name: "Tugas",
    icon: "nc-icon nc-circle-09",
    component: DataUser,
  },
  {
    path: "/History",
    name: "Riwayat",
    icon: "nc-icon nc-notes",
    component: DataAdmin,
  },
];

export default WorkerRoute;
