import { Container } from "react-bootstrap";
import Table from "react-bootstrap/Table";
import Activity from "../../assets/img/actionyes.svg";
import ActivityDel from "../../assets/img/activitytrash.svg";
import Button from "react-bootstrap";
import search from "../../assets/img/search.svg";
import Nav from "react-bootstrap/Nav";
import logo from "../../assets/img/logo-kuning.svg";
import React from "react";
import Form from "react-bootstrap/Form";
import NavDropdown from "react-bootstrap/NavDropdown";

function Navbar() {
  return (
    <div className="content">
      <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">

        <Container>
          <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link href="#features">Features</Nav.Link>
              <Nav.Link href="#pricing">Pricing</Nav.Link>
              <NavDropdown title="Dropdown" id="collasible-nav-dropdown">
                <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.2">
                  Another action
                </NavDropdown.Item>
                <NavDropdown.Item href="#action/3.3">
                  Something
                </NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="#action/3.4">
                  Separated link
                </NavDropdown.Item>
              </NavDropdown>
            </Nav>
            <Nav>
              <Nav.Link href="#deets">More deets</Nav.Link>
              <Nav.Link eventKey={2} href="#memes">
                Dank memes
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>{" "}    
      
          </div>
  );
}

export default Navbar;
