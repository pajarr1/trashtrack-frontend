import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Index from '../pages/app/Index'

function MainRoutes() {
    return (
        <BrowserRouter>
            <Switch>
            <Route path="/">
                <Index />
            </Route>
            </Switch>
        </BrowserRouter>

     );
}

export default MainRoutes;