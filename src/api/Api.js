
import axios from 'axios'

const api = 'http://localhost:3333/api/v1/'

// login api
export const loginApi = async (req) => {
    try {
        const response = await axios.post(`${api}login`, req);

        return response.data;
    } catch (error) {
        return error;
    }
}

// register api
export const registerApi = async (req) => {
    try {
        const response = await axios.post(`${api}register`, req);

        return response.data;
    } catch (error) {
        return error;
    }
}


// get data user api
export const dataUser = async () => {
    try {
        const response = await axios.get(`${api}list_users`);

        return response.data;
    } catch (error) {
        return error;
    }
}

// insert data user api
export const insertUser = async (req) => {
    try {
        const response = await axios.post(`${api}add_user`, req);

        return response.data;
    } catch (error) {
        return error;
    }
}

// delete data user api
export const userDeleteApi = async (id) => {
    try {
        var config = {
            method: 'post',
            maxBodyLength: Infinity,
        url: `${api}delete_users/?id=${id}`,
            headers: {
                'Content-Type': 'multipart/form-data'
              },
          };

        const response = await axios(config);

        return response.data;
    } catch (error) {
        return error;
    }
}

// data task pekerja
export const taskPekerja = async () => {
    try {
        const response = await axios.get(`${api}worker_task`);

        return response.data;
    } catch (error) {
        return error;
    }
}


// chart dashboard
export const chartApi = async () => {
    try {
        const response = await axios.get(`${api}chart_trash`);
        return response.data;
    } catch (error) {
        return error;
    }
}

// chart dashboard
export const chartTersisaApi = async () => {
    try {
        const response = await axios.get(`${api}chart_tersisa`);
        return response.data;
    } catch (error) {
        return error;
    }
}

// chart dashboard
export const listPekerja = async () => {
    try {
        const response = await axios.get(`${api}list_worker`);
        return response.data;
    } catch (error) {
        return error;
    }
}

// add_task
export const insertTask = async (req) => {
    try {
        const response = await axios.post(`${api}add_task`, req);

        return response.data;
    } catch (error) {
        return error;
    }
}

export const checkCeklis = async (req) => {
    try {
        const response = await axios.post(`${api}add_task`, req);

        return response.data;
    } catch (error) {
        return error;
    }
}

// show trash

export const showTrash = async (req) => {
    try {
        const response = await axios.get(`${api}show_trash`);

        return response.data;
    } catch (error) {
        return error;
    }
}

// show task
export const showTask = async (req) => {
    try {
        const response = await axios.get(`${api}worker_task`);

        return response.data;
    } catch (error) {
        return error;
    }
}

// history insert
export const updateBeratDanStatusMenjadiSudahDiangkut = async (req) => {
    console.log(req)
    try {
        const response = await axios.post(`${api}update_status`, req);

        return response.data;
    } catch (error) {
        return error;
    }
}

// cek berat sampah
export const checkBerat = async (req) => {
    console.log(req)
    try {
        const response = await axios.post(`${api}check_berat`, req);

        return response.data;
    } catch (error) {
        return error;
    }
}

export const jumlahPekerja = async() => {
    try {
        const response = await axios.get(`${api}jumlah_pekerja`);
        return response.data;
    } catch (error) {
        return error;
    }
}

export const jumlahSampahPerminggu = async() => {
    try {
        const response = await axios.get(`${api}berat_sampah`);
        return response.data;
    } catch (error) {
        return error;
    }
}

export const jumlahSampahOrganik = async() => {
    try {
        const response = await axios.get(`${api}sampah_organik`);
        return response.data;
    } catch (error) {
        return error;
    }
}

export const jumlahSampahAnorganik = async() => {
    try {
        const response = await axios.get(`${api}sampah_anorganik`);
        return response.data;
    } catch (error) {
        return error;
    }
}

export const jumlahSampahRumahTangga = async() => {
    try {
        const response = await axios.get(`${api}sampah_rumahtangga`);
        return response.data;
    } catch (error) {
        return error;
    }
}
