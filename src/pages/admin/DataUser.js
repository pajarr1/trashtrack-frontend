import { useEffect, useState } from "react";
import { Button, Container } from "react-bootstrap";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import Table from "react-bootstrap/Table";
import add from "../../assets/img/add.svg";
import search from "../../assets/img/search.svg";
import ActivityDel from "../../assets/img/activitytrash.svg";
import { dataUser, insertUser,userDeleteApi } from "../../api/Api";
import { toast, ToastContainer } from "react-toastify";

function DataUser() {
  const [state, setState] = useState([]); // data api user
  const [show, setShow] = useState(false); // modal show

  const [username, setUsername] = useState()
  const [email, setEmail] = useState()
  const [role, setRole] = useState()
  const [password, setPassword] = useState()

  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);

  const submitForm = () => {
    const datares = insertUser({
      username,
      email,
      role,
      password
    })
    toast("Berhasil di submit!", {
      position: toast.POSITION.BOTTOM_RIGHT,
      className: 'foo-bar'
    })
    dataUser();
    handleClose()
    window.location.reload()
  }


  const deleteUser = (id) => {
    if(window.confirm('delete user?')) {
      const res = userDeleteApi(id)
      // toast.success('delete success')
      console.log(res)
      // dataUser();
    // handleClose()
    //   window.location.reload()
    }
  }

  useEffect(() => {
    const fetchApi = async () => {
      const dataus = await dataUser();

      // slicing
      // const sliceData = dataus.slice(0, 6);

      setState(dataus);
    };

    fetchApi();
  });

  return (
    <div className="content">
      <Container fluid>
        <span className="fs-4 fw-bold">Data User</span>
        <button
          type="button"
          onClick={handleShow}
          className="btn btn-primary rounded-pill float-end"
          style={{ hover: 'none'}}
        >
          <img src={add} className="mb-1" />
          <span className="fs-7 fw-bold mx-2">Tambah</span>
        </button>{" "}
        <br />
        <span className="fs-4">Home / Data user</span>
        <div className="mt-5">
          <span className="fs-7">Show</span>{" "}
          <button type="button" className="btn btn-primary rounded-pill">
            10
          </button>{" "}
          <span className="fs-7 ps-1">Entire</span>
          <div className="d-flex float-end">
            <img src={search} className="mx-2"/>{" "}
            <input
              type="text"
              id=""
              className="form-control w-100"
              aria-describedby="passwordHelpBlock"
            />
          </div>
        </div>
        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Pengelolaan User</Modal.Title>
          </Modal.Header>
            <h6 style={{ marginLeft: '20px'}}>Home / Data User / Tambah Pekerja</h6>
          <Modal.Body>
            <Form>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                <Form.Label>Username</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter your name"
                  onChange={(e) => setUsername(e.target.value)}
                  autoFocus
                />
              </Form.Group>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                <Form.Label>Email address</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="name@example.com"
                  onChange={(e) => setEmail(e.target.value)}
                  autoFocus
                />
              </Form.Group>
              <Form.Group
                className="mb-3"
                controlId="exampleForm.ControlInput1"
              >
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Enter your password"
                  onChange={(e) => setPassword(e.target.value)}
                  autoFocus
                />
              </Form.Group>
              <Form.Group controlId="formBasicSelect">
                <Form.Label>Select Role</Form.Label>
                <Form.Control
                  as="select"
                  onChange={e => {
                    console.log("e.target.value", e.target.value);
                    setRole(e.target.value);
                  }}
                >
                  <option value="petugas" selected>Petugas</option>
                  <option value="masyarakat">Masyarakat</option>
                </Form.Control>
              </Form.Group>

            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={submitForm}>
              Save Changes
            </Button>
          </Modal.Footer>
        </Modal>
        <div className="card mt-3 border-0 shadow">
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Id</th>
              <th>Username</th>
              <th>Email</th>
              <th>Role</th>
              <th>Last Activity</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {state.map((value, index) => {
              return (
                <tr>
                  <td>{value.id}</td>
                  <td>{value.id}</td>
                  <td>{value.username}</td>
                  <td>{value.email}</td>
                  <td>{value.role}</td>
                  <td>8 Second Ago</td>
                    <td onClick={() => deleteUser(value.id)}><img src={ActivityDel} /></td>
                </tr>
              );
            })}
          </tbody>
          <ToastContainer autoClose={8000} />
        </Table>
        </div>
      </Container>
    </div>
  );
}

export default DataUser;
