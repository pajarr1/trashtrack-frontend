import Activity from "../../assets/img/actionyes.svg";
import ActivityDel from "../../assets/img/activitytrash.svg";
import search from "../../assets/img/search.svg";
import { useEffect, useState } from "react";
import {
  Badge,
  Button,
  Card,
  Navbar,
  Nav,
  Table,
  Container,
  Row,
  Col,
  Form,
  OverlayTrigger,
  Tooltip,
  Carousel,
  Modal,
  Dropdown,
} from "react-bootstrap";
import { insertTask, listPekerja, showTrash } from "../../api/Api";
import { toast, ToastContainer } from "react-toastify";


function DataAdmin() {
  const [state, setState] = useState([])
  const [show, setShow] = useState(false);
  const [pekerja, setListPekerja] = useState([])
  const [pemilik, setPemilik] = useState()
  const [pekerjaan, setPekerja] = useState()
  const [tanggalMulai, setTanggalMulai] = useState()
  const [tanggalSelesai, setTanggalSelesai] = useState()

  useEffect(() => {
    const fetchApi = async () => {
      const dataus = await showTrash();

      // slicing
      // const sliceData = dataus.slice(0, 6);

      setState(dataus);
    };

    fetchApi();
  });

  const handleShow = async () => {
    const data = await listPekerja()
    setListPekerja(data)
    setShow(true)
  };
  const handleClose = () => setShow(false);

  const handlePekerjaList = (event,params) => {
    setPekerja(params)
  }
  const submitForm = async () => {
    handlePekerjaList()
    const restadata = await insertTask({
      pemilik_sampah: pemilik,
      pekerja: pekerjaan,
      tanggal_mulai: tanggalMulai,
      tanggal_selesai: tanggalSelesai,
    });

    // if(restadata.)
    if(restadata.success) {
      toast("Berhasil di submit!", {
        position: toast.POSITION.BOTTOM_RIGHT,
        className: 'foo-bar'
      })
    }
    // console.log('all', pemilik,pekerjaan,tanggalMulai,tanggalSelesai)
  }


  return (
    <div className="content">
      <Modal size="lg" show={show} onHide={handleClose}>
        <Modal.Header closeButton className="bg-green text-center">
          <center>
            <span className="fw-bold fs-6 text-white">Kirim Sampah</span>
          </center>
        </Modal.Header>

        <Modal.Body className="bg-green text-white">
          <Form>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Pemilik Sampah</Form.Label>
              <Form.Control
                type="text"
                placeholder="Nama Pemilik"
                required
                onChange={(e) => setPemilik(e.target.value)}
                autoFocus
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Pekerja</Form.Label>
              {/* <Form.Select className="font-14 shadow-none">
               {pekerja.map((val,key) => {
                return <option onClick={(event) => handlePekerjaList(event,val.id)}>{val.id}{val.username}</option>
               })}
              </Form.Select> */}
               <Dropdown>
                <Dropdown.Toggle variant="success" id="dropdown-basic">
                  Pekerja
                </Dropdown.Toggle>

                <Dropdown.Menu>
                  {pekerja.map((val,key) => {
                  return <Dropdown.Item onClick={(event) => handlePekerjaList(event,val.id)}>{val.username}</Dropdown.Item>
                })}

                </Dropdown.Menu>
              </Dropdown>
            </Form.Group>
            <Form.Label>Mulai</Form.Label>
            <Form.Label className="float-end">Selesai</Form.Label>
            <Form.Group
              className="mb-3 d-flex"
              controlId="exampleForm.ControlInput1"
            >
              <Form.Control
                type="date"
                placeholder=""
                onChange={(e) => setTanggalMulai(e.target.value)}
                autoFocus
                className="w-50"
              />

              <Form.Control
                type="date"
                placeholder=""
                className="w-50"
                onChange={(e) => setTanggalSelesai(e.target.value)}
                autoFocus
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer className="bg-green">
          <Button
            variant="primary"
            className="btn-green w-100 rounded-pill text-white fw-bold"
            onClick={submitForm}
          >
            Simpan
          </Button>
        </Modal.Footer>
      </Modal>
      <Container fluid>
        <span className="fs-4 fw-bold ">Data Sampah</span>
        <br />
        <span className="fs-4 fw-normal ">Home / Data Sampah</span>
        <br />
        <div className="mt-5">
          <span className="fs-7 ">Show</span>{" "}
          <button type="button" className="btn btn-primary rounded-pill">
            10
          </button>{" "}
          <span className="fs-7 ps-1">Entire</span>
          <div className="d-flex float-end">
            <img src={search} className="mx-3" />{" "}
            <input
              type="text"
              id=""
              className="form-control w-100"
              aria-describedby="passwordHelpBlock"
            />
          </div>
        </div>
        <div className="card rounded border-0 shadow mt-3">
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>No</th>
                <th>Pemilik Sampah</th>
                <th>Tanggal</th>
                <th>Lokasi</th>
                <th>Status</th>
                <th>Activity</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>

              {/* proses, menunggu,selesai */}
              {state.map((value, index) => {
              return (
               <tr>
                <td>{value.id}</td>
                <td>{value.nama_pembuang}</td>
                <td>{value.tanggal}</td>
                <td>{value.lokasi_sampah}</td>
                <td>{value.status}</td>
                <td className="text-warning">Proses</td>
                <td>
                  <img src={Activity} className="mx-3" onClick={handleShow} />
                  <img src={ActivityDel} />
                </td>
                </tr>
              );
            })}
            </tbody>
          </Table>
          <ToastContainer autoClose={8000} />
        </div>
      </Container>
    </div>
  );
}

export default DataAdmin;
