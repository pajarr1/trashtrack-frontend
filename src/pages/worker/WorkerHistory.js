import React, { useState } from "react";
// react-bootstrap components
import {
  Badge,
  Button,
  Card,
  Navbar,
  Nav,
  Table,
  Container,
  Row,
  Col,
  Form,
  OverlayTrigger,
  Tooltip,
  Carousel,
} from "react-bootstrap";
import lokasi from "../../assets/img/location.png";
import Alert from "react-bootstrap/Alert";
import carousel1 from "../../assets/img/3.jpeg";
import carousel2 from "../../assets/img/3.jpeg";
import dump from "../../assets/img/dump.png";
import checksign from "../../assets/img/check.svg";

function WorkerHistory() {
  const [show, setShow] = useState(true);
  return (
    <div>
      <div className="content">
        <Container fluid>
          {" "}
          <span className="fs-3 fw-bold text-primary">History</span>
          <br />
          <span className="fs-5 fw-normal text-green">Dashboard/History</span>
          <Row>
            <Col md={3  }>
              {" "}
              <Card
                className="shadow border-0 m-2 cursor-pointer"
                style={{ maxWidth: "250px", height: "200px" }}
              >
                <center>
                  <Card.Img
                    className="w-50 item-center mt-3"
                    src={dump}
                    style={{ minHeight: "90px" }}
                  />
                </center>
                <Card.Body className="">
                  <span className="fs-6 fw-bold">Pengirim :</span>{" "}
                  <span className="fs-6 fw-bold float-end">Minji</span>
                  <Button
                    variant="primary"
                    className=" mt-2 btn-primary  w-100 text-white font-14"
                    type="submit"
                  >
                    Detail
                  </Button>
                </Card.Body>
              </Card>
            </Col>
            <Col md={3  }>
              {" "}
              <Card
                className="shadow border-0 m-2 cursor-pointer"
                style={{ maxWidth: "250px", height: "200px" }}
              >
                <center>
                  <Card.Img
                    className="w-50 item-center mt-3"
                    src={dump}
                    style={{ minHeight: "90px" }}
                  />
                </center>
                <Card.Body className="">
                  <span className="fs-6 fw-bold">Pengirim :</span>{" "}
                  <span className="fs-6 fw-bold float-end">Minji</span>
                  <Button
                    variant="primary"
                    className=" mt-2 btn-primary  w-100 text-white font-14"
                    type="submit"
                  >
                    Detail
                  </Button>
                </Card.Body>
              </Card>
            </Col>
            <Col md={3  }>
              {" "}
              <Card
                className="shadow border-0 m-2 cursor-pointer"
                style={{ maxWidth: "250px", height: "200px" }}
              >
                <center>
                  <Card.Img
                    className="w-50 item-center mt-3"
                    src={dump}
                    style={{ minHeight: "90px" }}
                  />
                </center>
                <Card.Body className="">
                  <span className="fs-6 fw-bold">Pengirim :</span>{" "}
                  <span className="fs-6 fw-bold float-end">Minji</span>
                  <Button
                    variant="primary"
                    className=" mt-2 btn-primary  w-100 text-white font-14"
                    type="submit"
                  >
                    Detail
                  </Button>
                </Card.Body>
              </Card>
            </Col>{" "}
            <Col md={3  }>
              {" "}
              <Card
                className="shadow border-0 m-2 cursor-pointer"
                style={{ maxWidth: "250px", height: "200px" }}
              >
                <center>
                  <Card.Img
                    className="w-50 item-center mt-3"
                    src={dump}
                    style={{ minHeight: "90px" }}
                  />
                </center>
                <Card.Body className="">
                  <span className="fs-6 fw-bold">Pengirim :</span>{" "}
                  <span className="fs-6 fw-bold float-end">Minji</span>
                  <Button
                    variant="primary"
                    className=" mt-2 btn-primary  w-100 text-white font-14"
                    type="submit"
                  >
                    Detail
                  </Button>
                </Card.Body>
              </Card>
            </Col>{" "}
            <Col md={3  }>
              {" "}
              <Card
                className="shadow border-0 m-2 cursor-pointer"
                style={{ maxWidth: "250px", height: "200px" }}
              >
                <center>
                  <Card.Img
                    className="w-50 item-center mt-3"
                    src={dump}
                    style={{ minHeight: "90px" }}
                  />
                </center>
                <Card.Body className="">
                  <span className="fs-6 fw-bold">Pengirim :</span>{" "}
                  <span className="fs-6 fw-bold float-end">Minji</span>
                  <Button
                    variant="primary"
                    className=" mt-2 btn-primary  w-100 text-white font-14"
                    type="submit"
                  >
                    Detail
                  </Button>
                </Card.Body>
              </Card>
            </Col>{" "}
            <Col md={3  }>
              {" "}
              <Card
                className="shadow border-0 m-2 cursor-pointer"
                style={{ maxWidth: "250px", height: "200px" }}
              >
                <center>
                  <Card.Img
                    className="w-50 item-center mt-3"
                    src={dump}
                    style={{ minHeight: "90px" }}
                  />
                </center>
                <Card.Body className="">
                  <span className="fs-6 fw-bold">Pengirim :</span>{" "}
                  <span className="fs-6 fw-bold float-end">Minji</span>
                  <Button
                    variant="primary"
                    className=" mt-2 btn-primary  w-100 text-white font-14"
                    type="submit"
                  >
                    Detail
                  </Button>
                </Card.Body>
              </Card>
            </Col>{" "}
            <Col md={3  }>
              {" "}
              <Card
                className="shadow border-0 m-2 cursor-pointer"
                style={{ maxWidth: "250px", height: "200px" }}
              >
                <center>
                  <Card.Img
                    className="w-50 item-center mt-3"
                    src={dump}
                    style={{ minHeight: "90px" }}
                  />
                </center>
                <Card.Body className="">
                  <span className="fs-6 fw-bold">Pengirim :</span>{" "}
                  <span className="fs-6 fw-bold float-end">Minji</span>
                  <Button
                    variant="primary"
                    className=" mt-2 btn-primary  w-100 text-white font-14"
                    type="submit"
                  >
                    Detail
                  </Button>
                </Card.Body>
              </Card>
            </Col>{" "}
            <Col md={3  }>
              {" "}
              <Card
                className="shadow border-0 m-2 cursor-pointer"
                style={{ maxWidth: "250px", height: "200px" }}
              >
                <center>
                  <Card.Img
                    className="w-50 item-center mt-3"
                    src={dump}
                    style={{ minHeight: "90px" }}
                  />
                </center>
                <Card.Body className="">
                  <span className="fs-6 fw-bold">Pengirim :</span>{" "}
                  <span className="fs-6 fw-bold float-end">Minji</span>
                  <Button
                    variant="primary"
                    className=" mt-2 btn-primary  w-100 text-white font-14"
                    type="submit"
                  >
                    Detail
                  </Button>
                </Card.Body>
              </Card>
            </Col>{" "}
            <Col md={3  }>
              {" "}
              <Card
                className="shadow border-0 m-2 cursor-pointer"
                style={{ maxWidth: "250px", height: "200px" }}
              >
                <center>
                  <Card.Img
                    className="w-50 item-center mt-3"
                    src={dump}
                    style={{ minHeight: "90px" }}
                  />
                </center>
                <Card.Body className="">
                  <span className="fs-6 fw-bold">Pengirim :</span>{" "}
                  <span className="fs-6 fw-bold float-end">Minji</span>
                  <Button
                    variant="primary"
                    className=" mt-2 btn-primary  w-100 text-white font-14"
                    type="submit"
                  >
                    Detail
                  </Button>
                </Card.Body>
              </Card>
            </Col>{" "}
            <Col md={3  }>
              {" "}
              <Card
                className="shadow border-0 m-2 cursor-pointer"
                style={{ maxWidth: "250px", height: "200px" }}
              >
                <center>
                  <Card.Img
                    className="w-50 item-center mt-3"
                    src={dump}
                    style={{ minHeight: "90px" }}
                  />
                </center>
                <Card.Body className="">
                  <span className="fs-6 fw-bold">Pengirim :</span>{" "}
                  <span className="fs-6 fw-bold float-end">Minji</span>
                  <Button
                    variant="primary"
                    className=" mt-2 btn-primary  w-100 text-white font-14"
                    type="submit"
                  >
                    Detail
                  </Button>
                </Card.Body>
              </Card>
            </Col>{" "}
            <Col md={3  }>
              {" "}
              <Card
                className="shadow border-0 m-2 cursor-pointer"
                style={{ maxWidth: "250px", height: "200px" }}
              >
                <center>
                  <Card.Img
                    className="w-50 item-center mt-3"
                    src={dump}
                    style={{ minHeight: "90px" }}
                  />
                </center>
                <Card.Body className="">
                  <span className="fs-6 fw-bold">Pengirim :</span>{" "}
                  <span className="fs-6 fw-bold float-end">Minji</span>
                  <Button
                    variant="primary"
                    className=" mt-2 btn-primary  w-100 text-white font-14"
                    type="submit"
                  >
                    Detail
                  </Button>
                </Card.Body>
              </Card>
            </Col>{" "}
            <Col md={3  }>
              {" "}
              <Card
                className="shadow border-0 m-2 cursor-pointer"
                style={{ maxWidth: "250px", height: "200px" }}
              >
                <center>
                  <Card.Img
                    className="w-50 item-center mt-3"
                    src={dump}
                    style={{ minHeight: "90px" }}
                  />
                </center>
                <Card.Body className="">
                  <span className="fs-6 fw-bold">Pengirim :</span>{" "}
                  <span className="fs-6 fw-bold float-end">Minji</span>
                  <Button
                    variant="primary"
                    className=" mt-2 btn-primary  w-100 text-white font-14"
                    type="submit"
                  >
                    Detail
                  </Button>
                </Card.Body>
              </Card>
            </Col>{" "}
          </Row>
        </Container>
      </div>
    </div>
  );
}

export default WorkerHistory;
