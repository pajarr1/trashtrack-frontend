import React, { useEffect, useState } from "react";
// react-bootstrap components
import {
  Badge,
  Button,
  Card,
  Navbar,
  Nav,
  Table,
  Container,
  Modal,
  Row,
  Col,
  Form,
  OverlayTrigger,
  Tooltip,
  Carousel,
} from "react-bootstrap";
import lokasi from "../../assets/img/location.png";
import Alert from "react-bootstrap/Alert";
import carousel1 from "../../assets/img/3.jpeg";
import carousel2 from "../../assets/img/3.jpeg";
import carousel3 from "../../assets/img/3.jpeg";
import checksign from "../../assets/img/check.svg";
import { checkBerat, taskPekerja, updateBeratDanStatusMenjadiSudahDiangkut } from "../../api/Api";
import { confirmAlert } from "react-confirm-alert";

function WorkerAssignment() {
  const [state, setState] = useState([])
  const [show, setShow] = useState(true);
  const [beratSampah, setBeratSampah] = useState();                   // auto field dan bisa diganti custom
  const [tanggalPenyelesaiaan, setTanggalPenyelesaiaan] = useState(); // form

  const [idPengirim, setIdPengirim] = useState()


  // get longlat
  // useEffect(() => {
  //   navigator.geolocation.getCurrentPosition(
  //     function(position) {
  //       console.log(position.coords.latitude);
  //     },
  //     function(error) {
  //       console.error("Error Code = " + error.code + " - " + error.message);
  //     }
  //   );
  // })

  const handleShow = async (event, params) => {
    const check= await checkBerat(params)
    const chars = {
      'K': '',
      'G': '',
      ',': '.',
      'k': '',
      'g': '',
    };


    const final = check[0].berat_sampah


    setIdPengirim(check[0].id)

    const s = final.replace(/[KG,kg]/g, m => chars[m]);
    setBeratSampah(s)
    setShow(true)
  };

  const handleClose = () => setShow(false);

  const submitForm = async () => {
    const data = await updateBeratDanStatusMenjadiSudahDiangkut({id: idPengirim, berat_sampah: beratSampah, tanggal_selesai: tanggalPenyelesaiaan})
    console.log(data)
    // console.log('all', pemilik,pekerjaan,tanggalMulai,tanggalSelesai)
  }


  const confirmTaking = () => {
    confirmAlert  ({
      title: 'Confirm to submit',
      message: 'Are you sure to do this.',
      buttons: [
        {
          label: 'Yes',
          onClick: () => alert('Click Yes')
        },
        {
          label: 'No',
          onClick: () => alert('Click No')
        }
      ]
    });
  }

  useEffect(() => {
    const fetchApi = async () => {
      const dataus = await taskPekerja();

      setState(dataus);
    };

    fetchApi();
  },5000);

  function CheckStatus(props) {
    const check = props.data.tanggal_selesai
    // console.log(check)

    if(check !== null) {
      return  (
        <div className="card rounded bg-primary mt-1" onClick={event => handleShow(event, props.data)}>
              <div className=" w-100 text-white mt-2 mb-2">
                <span className="mb-2 fs-5 ms-3 fw-bold mt-2">
                  Pengirims : {props.data.pemilik_sampah}
                </span>
                <span className="fs-5 fw-bold float-end mx-3" onClick={confirmTaking}>
                    Angkut Sekarang
                  <img src={checksign} className="mx-3" />
                </span>
              </div>
            </div>
      )

    }

    if(check === null) {
      return  (
        <div className="card rounded bg-warning mt-1">
              <div className=" w-100 text-white mt-2 mb-2">
                <span className="mb-2 fs-5 ms-3 fw-bold mt-2">
                  Pengirim : {props.data.pemilik_sampah}
                </span>
                <span className="fs-5 fw-bold float-end mx-3" onClick={confirmTaking}>
                    Pandu Sekarang
                  <img src={checksign} className="mx-3" />
                </span>
              </div>
            </div>
      )
    }
  }

  return (
    <div>
      <div className="content">

      <Modal size="lg" show={show} onHide={handleClose}>
        <Modal.Header closeButton className="bg-green text-center">
          <center>
            <span className="fw-bold fs-6 text-white">Selesaikan</span>
          </center>
        </Modal.Header>

        <Modal.Body className="bg-green text-white">
          <Form>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Total Berat Sampah</Form.Label>
              <Form.Control
                type="number"
                required
                value={beratSampah}
                onChange={(e) => setBeratSampah(e.target.value)}
                autoFocus
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Tanggal Penyelesaiaan</Form.Label>
              <Form.Control
                type="date"
                placeholder=""
                onChange={(e) => setTanggalPenyelesaiaan(e.target.value)}
                autoFocus
                className="w-100"
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer className="bg-green">
          <Button
            variant="primary"
            className="btn-green w-100 rounded-pill text-white fw-bold"
            onClick={submitForm}
          >
            Simpan
          </Button>
        </Modal.Footer>
      </Modal>

        <Container fluid>
          {" "}
          <span className="fs-3 fw-bold text-primary">Tugas</span>
          <br />
          <span className="fs-5 fw-normal text-green">Dashboard/Tugas</span>

          {state.map((value) => {
            return (

            <CheckStatus data={value} />
            )
          })}


          {/* <div className="card rounded bg-warning mt-1">
            <div className=" w-100 text-white mt-2 mb-2">
              <span className="mb-2 fs-5 ms-3 fw-bold mt-2">
                Pengirim : Bunga
              </span>
              <span className="fs-5 fw-bold float-end mx-3">
                Pandu Sekarang
                <img src={checksign} className="mx-3" />
              </span>
            </div>
          </div> */}
        </Container>
      </div>
    </div>
  );
}

export default WorkerAssignment;
