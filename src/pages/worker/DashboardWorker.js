import React, { useState } from "react";
// react-bootstrap components
import {
  Badge,
  Button,
  Card,
  Navbar,
  Nav,
  Table,
  Container,
  Row,
  Col,
  Form,
  OverlayTrigger,
  Tooltip,
  Carousel,
} from "react-bootstrap";
import lokasi from "../../assets/img/location.png";
import Alert from "react-bootstrap/Alert";
import carousel1 from "../../assets/img/3.jpeg";
import carousel2 from "../../assets/img/3.jpeg";
import carousel3 from "../../assets/img/3.jpeg";

function DashboardWorker() {
  const [show, setShow] = useState(true);
  return (
    <div>
      <div className="content">
        <Container fluid>
          {" "}
          {/* <h3 className="fw-bold text-primary">Dashboard Pekerja</h3> */}
          <center>
            <Carousel className="w-75 ">
              <Carousel.Item interval={1000}>
                <img
                  className="d-block w-100"
                  src={carousel1}
                  // alt="First slide"
                />
              </Carousel.Item>
              <Carousel.Item interval={500}>
                <img
                  className="d-block w-100"
                  src={carousel2}
                  // alt="Second slide"
                />
              </Carousel.Item>
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src={carousel3}
                  // alt="Third slide"
                />
              </Carousel.Item>
            </Carousel>
          </center>
          <div className="card rounded bg-primary mt-3">
            <div className=" w-100 text-white mt-2 mb-2">
              <span className="mb-2 fs-5 ms-3 fw-bold mt-2">
                Ada sampah yang menunggumu!!!
              </span>
              <span className="fs-5 fw-bold float-end mx-3">
                Lihat Sekarang!{" "}
              </span>
            </div>
          </div>
          <Row>
            <Col md="6">
              <Card>
                <Card.Header>
                  <Card.Title as="h4" className="fw-semibold">
                    Sampah Tersisa 7 hari terakhir{" "}
                  </Card.Title>
                </Card.Header>
                <Card.Body>
                  <div className="ct-chart" id="chartActivity"></div>
                </Card.Body>
                <Card.Footer>
                  <div className="legend">
                    <i className="fas fa-circle text-info"></i>
                    <i className="fas fa-circle text-danger"></i>
                  </div>
                  <hr></hr>
                  <div className="stats">
                    <i className="fas fa-check"></i>
                    Data information certified
                  </div>
                </Card.Footer>
              </Card>
            </Col>
            <Col md="6">
              <Card>
                <Card.Header>
                  <Card.Title className="d-flex">
                    <h5 className="float-end fw-semibold">Sampah Diangkut</h5>
                    {/* <span>Hari</span>
                      <span>Minggu</span>
                      <span>Bulan</span> */}
                  </Card.Title>
                </Card.Header>
                <Card.Body>
                  <div className="ct-chart" id="chartActivity"></div>
                </Card.Body>
                <Card.Footer>
                  <div className="legend">
                    <i className="fas fa-circle text-info"></i>
                    <i className="fas fa-circle text-danger"></i>
                  </div>
                  <hr></hr>
                  <div className="stats">
                    <i className="fas fa-check"></i>
                    Data information certified
                  </div>
                </Card.Footer>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
}

export default DashboardWorker;
