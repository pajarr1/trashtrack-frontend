import React, { useState } from "react";
import axios from "axios";
import { Button } from "react-bootstrap";
import person from "../../assets/img/Undraw.svg";
import logo from "../../assets/img/trashtracklogo.svg";
import { useHistory } from "react-router-dom";
import { loginApi } from "../../api/Api";

async function loginUser(credentials) {
  const dataLogin = await loginApi(credentials)
    // setup localstorage
    localStorage.setItem('token', dataLogin.token)
    localStorage.setItem('user_id', dataLogin.user.id)
    localStorage.setItem('username', dataLogin.user.username)
    window.location.href = "http://localhost:3000/dashboard"
    alert('Login successfully.')
}

export default function Login() {
  const history = useHistory();
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();

  const handleSubmit = async (e) => {
    e.preventDefault();
    await loginUser({
      email,
      password,
    });
  };

  return (
    <div
      className="w-100 position-absolute img-bg-top"
      style={{ maxWidth: "100%", height: "500px" }}
    >
      <div className="container">
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <img src={person} className="w-50" />
        <div className="card w-50  shadow border-0 container float-end">
          <div className="">
            <form onSubmit={handleSubmit}>
              <h5 className="mt-3 ms-5 text-right">Sign in to Trash Track</h5>
              <span className="mt-3 ms-5 text-right">New Here?</span>{" "}
              <span
                className="mt-3 text-right text-primary "
                onClick={() =>
                  history.go("/register")
                }
              >
                Create an account
              </span>
              <center>
                <div class="mb-3 mt-3 content-center">
                  <input
                    type="email"
                    placeholder="Email"
                    autocomplete="off"
                    className="form-control w-75 font-14"
                    id="exampleInputText1"
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </div>

                <div className="mb-3">
                  <input
                    type="password"
                    placeholder="Password"
                    autocomplete="off"
                    className="form-control w-75 font-14"
                    id="exampleInputText1"
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </div>
              </center>
              {/* <label>
                <p>Username</p>
                <input
                  type="text"
                  className="font-bold text-primary"
                  onChange={(e) => setEmail(e.target.value)}
                />
              </label>
              <label>
                <p>Password</p>
                <input
                  type="password"
                  onChange={(e) => setPassword(e.target.value)}
                />
              </label> */}
              <center>
                <div className="">
                  <Button
                    as="input"
                    type="submit"
                    variant="primary"
                    className="w-75 mt-2 mb-5"
                    value="Sign In"
                  />
                </div>
                <img src={logo} className="" width="50" />
              </center>
              <br />
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
