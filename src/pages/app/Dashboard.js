import React, { useState } from "react";
// react-bootstrap components
import {
  Badge,
  Button,
  Card,
  Navbar,
  Nav,
  Table,
  Container,
  Row,
  Col,
  Form,
  OverlayTrigger,
  Tooltip,
  Modal,
} from "react-bootstrap";
import lokasi from "../../assets/img/location.png";
import Alert from "react-bootstrap/Alert";
import { GoogleMap, Marker, MarkerF, useLoadScript } from "@react-google-maps/api";
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import { useEffect } from "react";
import { 
  chartApi,
  chartTersisaApi,
  jumlahPekerja,
  jumlahSampahPerminggu,
  jumlahSampahAnorganik,
  jumlahSampahOrganik,
  jumlahSampahRumahTangga
} from "../../api/Api";


function Dashboard() {
  const [show, setShow] = useState(true);
  const [pekerja, setPekerja] = useState([])
  const [sampahPerminggu, setSampahPerminggu] = useState([])
  const [sampahOrganik, setSampahOrganik] = useState([])
  const [sampahAnorganik, setSampahAnorganik] = useState([])
  const [sampahRumahTangga, setSampahRumahTangga] = useState([])
  // latitude
  const [latitude, setLatitude] = useState()
  const [longitude, setLongitude] = useState()

  // data chart
  const [databelum, setDataBelum] = useState([]);
  const [datasudah, setDataSudah] = useState([]);
  const [options, setOptions] = useState();
  const [barOptions, setBarOption] = useState();

  const [modalShow, setModalShow] = useState(true);
  const handleShow = () => setModalShow(true);
  const handleClose = () => setModalShow(false);

  const [zoom, setZoom] = useState(10)

  useEffect(() => {
    const apiPekerja = async () => {
      const data = await jumlahPekerja();
      setPekerja(data);
    };

    const sampahPerminggu = async () => {
      const data = await jumlahSampahPerminggu();
      setSampahPerminggu(data);
    };

    const sampahOrganik = async () => {
      const data = await jumlahSampahOrganik();
      setSampahOrganik(data);
    };

    const sampahAnorganik = async () => {
      const data = await jumlahSampahAnorganik();
      setSampahAnorganik(data);
    };

    const sampahRumahTangga = async () => {
      const data = await jumlahSampahRumahTangga();
      setSampahRumahTangga(data);
    };

    const fetchApi = async () => {
      const dataRes = await chartApi()
      const dataBar = await chartTersisaApi()
      console.log(dataRes)

      setDataBelum(dataRes.semuasampah)
      setDataSudah(dataRes.selesaidiangkut)
       let margins = [];     // belum diangkut
       let takditerima = []; // sudah diangkut
       let barData = []; // bar data

         // set long lat from current location
         navigator.geolocation.getCurrentPosition(
              function(position) {
                console.log(position.coords.latitude);
                setLatitude(position.coords.latitude);
                setLongitude(position.coords.longitude);
              },
              function(error) {
                console.error("Error Code = " + error.code + " - " + error.message);
              }
            );

       // insert to array
       databelum.forEach((val,key) => {
            margins.push(val.count)
       })


        datasudah.forEach((val,key) => {
            takditerima.push(val.count)
       })

       dataBar.forEach((val, key) => {
            barData.push(val.count)
       })

        // convert to number
        let resultmargin = margins.map(i=>Number(i));
        let takditerimaresult = takditerima.map(i=> Number(i))
        let barDataResult = barData.map(i => Number(i))

        setBarOption({
                title: {
                    text: 'Sampah Tersisa 7 hari Akhir'
                },
                chart: {
                        type: 'column'
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                     }
                },
                series: [{
                        name: 'Sampah Masuk',
                        data: barDataResult
                },
            ]
        })



        setOptions({
          title: {
              text: 'Sampah Tersisa 7 hari Akhir'
            },
            plotOptions: {
              column: {
                pointPadding: 0.2,
                borderWidth: 0
              }
            },

            series: [{
              name: 'Belum Diangkut',
              data: resultmargin
            },
            {
              name: 'Sudah Diangkut',
              data: takditerimaresult
            },
          ]
      })

    }

    sampahOrganik();
    sampahAnorganik();
    sampahRumahTangga();
    sampahPerminggu();
    apiPekerja();
    fetchApi()
  }, 5000)

  const detailSampah = (id) => {
    setModalShow(true)
    setZoom(15)
  }

  const [userLat, setUserLat] = useState([
    {
      id: 1,
      lat: -6.654560,
      lng: 106.847070
    },
    {
      id: 2,
      lat: -6.628007,
      lng: 106.824478
    }
  ])

  // maps
  // apiKey

  const { isLoaded } = useLoadScript({
    googleMapsApiKey: process.env.REACT_APP_MAPS_API_KEY
  })

  if(!isLoaded) {
    return <div>Loading</div>
  }

  function Map() {
    return <GoogleMap center={{ lat: latitude, lng: longitude}} zoom={zoom} mapContainerClassName="map-container">
      {userLat.map((value,index) => {
        return <MarkerF draggable={true} onClick={() => detailSampah(value.id)} position={{ lat: value.lat, lng: value.lng }} />
      })}
    </GoogleMap>
  }
  return (
    <div>
      <div className="content">
        <Alert show={show} variant="success" className="w-100">
          <Alert.Heading>Apa Kabar?!</Alert.Heading>
          <p>
            "Inspirasi merupakan anugerah dari kerja keras dan fokus." - Helen
            Hanson
          </p>
          <hr />
          <div className="d-flex justify-content-end">
            <Button onClick={() => setShow(false)} variant="outline-success">
              Hide
            </Button>
          </div>
        </Alert>

        <Modal show={modalShow} onHide={handleClose} dialogClassName="modal-bottom">
          <Modal.Header closeButton>
            <Modal.Title> <h2>Kp. xxx 01/01Kel. xxxx</h2></Modal.Title>
          </Modal.Header>
            <h6 style={{ marginLeft: '20px'}}>Kec. xxx Kab/Kota xxx</h6>
          <Modal.Body>
            <h3>12,6Km</h3>
            <h4>Organik, Anorganik dan rumah tangga</h4>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="primary">
              Save Changes
            </Button>
          </Modal.Footer>
        </Modal>

        {!show && (
          <Button onClick={() => setShow(true)} className="mb-3 ms-3">
            Hi, Selamat datang di dashboard{" "}
          </Button>
        )}

        <Container fluid>
          <Row>
            <div className="d-flex overflow-scroll">
              <Col lg="3" sm="6">
                <Card className="card-stats bg-primary border-0 shadow">
                  <Card.Body>
                    <Row>
                      <Col xs="5">
                        <div className="icon-big text-center icon-blue">
                          <i className="nc-icon nc-chart text-brown"></i>
                        </div>
                      </Col>
                      <Col xs="7">
                        <div className="numbers">
                          <p className="card-category text-secondary">
                            Jumlah Pekerja
                          </p>
                          {
                            pekerja.map((value) => {
                              return (
                                <Card.Title as="h4" className="text-light ">
                                  {value.role}
                                </Card.Title>
                              )
                            })
                          }
                          
                        </div>
                      </Col>
                    </Row>
                  </Card.Body>
                  <Card.Footer>
                    <hr></hr>
                    <div className="stats">
                      <i className="fas fa-redo mr-1 text-secondary"></i>
                      Update
                    </div>
                  </Card.Footer>
                </Card>
              </Col>
              <Col lg="3" sm="6" className="ms-3">
                <Card className="card-stats shadow bg-light border-0">
                  <Card.Body>
                    <Row>
                      <Col xs="5">
                        <div className="icon-big text-center icon-warning">
                          <i className="nc-icon nc-light-3 text-success"></i>
                        </div>
                      </Col>
                      <Col xs="7">
                        <div className="numbers">
                          <p className="card-category ">Sampah Minggu Ini</p>
                          {
                            sampahPerminggu.map((value)=>{
                              return (
                                <Card.Title as="h4" className="">
                                   {value.berat}KG
                                </Card.Title>
                              )
                            })
                          }
                          
                        </div>
                      </Col>
                    </Row>
                  </Card.Body>
                  <Card.Footer>
                    <hr></hr>
                    <div className="stats">
                      <i className="far fa-calendar-alt mr-1"></i>
                      10 Februari 2023
                    </div>
                  </Card.Footer>
                </Card>
              </Col>
              <Col lg="3" sm="6" className="ms-3">
                <Card className="card-stats bg-light shadow border-0">
                  <Card.Body>
                    <Row>
                      <Col xs="5">
                        <div className="icon-big text-center icon-warning">
                          <i className="nc-icon nc-vector text-danger"></i>
                        </div>
                      </Col>
                      <Col xs="7">
                        <div className="numbers">
                          <p className="card-category">Sampah Organik</p>
                          {
                            sampahOrganik.map((value)=> {
                              return (
                                <Card.Title as="h4">{value.total}KG</Card.Title>
                              )
                            })
                          }
                          
                        </div>
                      </Col>
                    </Row>
                  </Card.Body>
                  <Card.Footer>
                    <hr></hr>
                    <div className="stats">
                      <i className="far fa-clock-o mr-1"></i>
                      In the last hour
                    </div>
                  </Card.Footer>
                </Card>
              </Col>
              <Col lg="3" sm="6" className="ms-3">
                <Card className="card-stats bg-light shadow border-0">
                  <Card.Body>
                    <Row>
                      <Col xs="5">
                        <div className="icon-big text-center icon-warning">
                          <i className="nc-icon nc-vector text-danger"></i>
                        </div>
                      </Col>
                      <Col xs="7">
                        <div className="numbers">
                          <p className="card-category">Sampah Anorganik</p>
                          {
                            sampahAnorganik.map((value)=> {
                              return (
                                <Card.Title as="h4">{value.total}KG</Card.Title>
                              )
                            })
                          }
                        </div>
                      </Col>
                    </Row>
                  </Card.Body>
                  <Card.Footer>
                    <hr></hr>
                    <div className="stats">
                      <i className="far fa-clock-o mr-1"></i>
                      In the last hour
                    </div>
                  </Card.Footer>
                </Card>
              </Col>
              <Col lg="3" sm="6" className="ms-3">
                <Card className="card-stats bg-light shadow border-0">
                  <Card.Body>
                    <Row>
                      <Col xs="5">
                        <div className="icon-big text-center icon-warning">
                          <i className="nc-icon nc-vector text-danger"></i>
                        </div>
                      </Col>
                      <Col xs="7">
                        <div className="numbers">
                          <p className="card-category">Sampah Rumah Tangga</p>
                          {
                            sampahRumahTangga.map((value)=> {
                              return (
                                <Card.Title as="h4">{value.total}KG</Card.Title>
                              )
                            })
                          }
                        </div>
                      </Col>
                    </Row>
                  </Card.Body>
                  <Card.Footer>
                    <hr></hr>
                    <div className="stats">
                      <i className="far fa-clock-o mr-1"></i>
                      In the last hour
                    </div>
                  </Card.Footer>
                </Card>
              </Col>
            </div>
          </Row>
          <Row className="mt-3">
            <Col>
              <Card>
                <Card.Header className="bg-green">
                  <Card.Title as="h5" className="mb-2 fw-bold text-white">
                    Lokasi Sampah
                  </Card.Title>
                </Card.Header>
                <Card.Body>
                  {/* <img src={lokasi} className=" w-100" /> */}
                  <Map />
                </Card.Body>
                <Card.Footer></Card.Footer>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col md="6">
              <Card>
                <Card.Header>
                  <Card.Title as="h4" className="fw-semibold">
                    Sampah Tersisa 7 hari terakhir{" "}
                  </Card.Title>
                </Card.Header>
                <Card.Body>
                <div className="ct-chart" id="chartHours">
                <HighchartsReact
                    highcharts={Highcharts}
                    chart="column"
                    options={options}
                />

                </div>
                </Card.Body>
                <Card.Footer>
                  <div className="legend">
                    <i className="fas fa-circle text-info"></i>
                    <i className="fas fa-circle text-danger"></i>
                  </div>
                  <hr></hr>
                  <div className="stats">
                    <i className="fas fa-check"></i>
                    Data information certified
                  </div>
                </Card.Footer>
              </Card>
            </Col>
            <Col md="6">
              <Card>
                <Card.Header>

                    <Card.Title className="d-flex">
                      <h5 className="float-end fw-semibold">Sampah Diangkut</h5>
                      {/* <span>Hari</span>
                      <span>Minggu</span>
                      <span>Bulan</span> */}
                    </Card.Title>

                </Card.Header>
                <Card.Body>
                  <div className="ct-chart" id="chartActivity">
                   <HighchartsReact
                                      highcharts={Highcharts}
                                      options={barOptions}
                                  />
                    </div>
                </Card.Body>
                <Card.Footer>
                  <div className="legend">
                    <i className="fas fa-circle text-info"></i>
                    <i className="fas fa-circle text-danger"></i>
                  </div>
                  <hr></hr>
                  <div className="stats">
                    <i className="fas fa-check"></i>
                    Data information certified
                  </div>
                </Card.Footer>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
}

export default Dashboard;
