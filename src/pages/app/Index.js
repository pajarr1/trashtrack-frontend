import React, {useState , useEffect} from "react";
import axios from 'axios';
import { Route, Switch } from "react-router-dom";
import DataUser from "../admin/DataUser";
import DataSampah from "../admin/DataSampah";
import Dashboard from "./Dashboard";
import Register from "./register";

import Sidebar from "../../Components/Sidebar/Sidebar";
import Navbar from "../../Components/Navbars/userNavbar";
import Header from "../../Components/Navbars/AdminNavbar";
import routes from "../../Components/Navbars/navRoute";
//import userRoute from '../../Components/Navbars/userRoute'
import petugasRoute from "../../Components/Navbars/WorkerRoute";
import sidebarImage from "../../assets/img/sidebar-3.jpg";
import DashboardWorker from "../worker/DashboardWorker";
import WorkerAssignment from "../worker/WorkerAssignment";
import WorkerHistory from "../worker/WorkerHistory";
import DashboarUser from "../user/DashboardUser";
import UserUpload from "../user/UserUpload";
import UserTunggu from "../user/UserTunggu";
import UserProses from "../user/UserProses";
import UserSelesai from "../user/UserSelesai";

const Index = () => {
  const [image, setImage] = React.useState(sidebarImage);
  const [hasImage, setHasImage] = React.useState(true);
  const [color, setColor] = React.useState("#BB8A52");
  const [username, setUsername] = useState();
  const [role, setRole] = useState()

   useEffect(() => {
      setUsername(localStorage.getItem("username"));

      axios
        .get("http://localhost:3333/api/v1/checkrole", {
          params: {
            user_id: localStorage.getItem("user_id"),
            username: localStorage.getItem("username"),
          },
        })
        .then((res) => {
          setRole(res.data.role);
        });
    }, 1000);


    function CheckSidebar(role) {
    if(role.role === 'user') {
          return <Navbar />
    } else {
              return <Sidebar color={color} routes={routes} petugasRoute={petugasRoute}/>
    }

    }

  return (
    <Switch>
      <div>
          <CheckSidebar role={role}/>
        <div className="main-panel">
          <div className="content">
            <Route path="/dashboard">
              <Dashboard />
            </Route>
            <Route path="/register">
              <Register />
            </Route>
            <Route path="/user">
              <DataUser />
            </Route>
            <Route path="/tes">tes</Route>
            <Route path="/sampah">
              <DataSampah />
            </Route>
            <Route path="/dashboard-worker">
              <DashboardWorker />
            </Route>
            <Route path="/assignment">
              <WorkerAssignment />
            </Route>
            <Route path="/history">
              <WorkerHistory />
            </Route>
            <Route path="/dashboard-user">
              <DashboarUser />
            </Route>
            <Route path="/user-upload">
              <UserUpload />
            </Route>
            <Route path="/user-tunggu">
              <UserTunggu />
            </Route>
            <Route path="/user-proses">
              <UserProses />
            </Route>
            <Route path="/user-selesai">
              <UserSelesai />
            </Route>
          </div>
        </div>
      </div>
    </Switch>
  );
};

export default Index;
