import React, { useState } from "react";
import axios from "axios";
import { Button } from "react-bootstrap";
import { registerApi } from "../../api/Api";
import person from "../../assets/img/Undraw.svg";
import logo from "../../assets/img/trashtracklogo.svg";
import { useHistory } from "react-router-dom";

async function registerFun(credentials) {
  const dataLogin = await registerApi(credentials)
    window.location.href = "http://localhost:3000/login"
    alert('Register successfully.')
}

export default function Register() {
  const history = useHistory();
  const [email, setEmail] = useState();
  const [username, setUsername] = useState();
  const [no_hp, setNomorHp] = useState();
  const [password, setPassword] = useState();
  const [passwordConfirm, setPasswordConfirm] = useState();

  const handleSubmit = async (e) => {
    e.preventDefault();

    if(passwordConfirm.localeCompare(password) === 0) {
      await registerFun({
        email,
        no_hp,
        username,
        password,
      });
    } else {
      alert('Password not match!')
    }
  };

  return (
    <div
      className="w-100 position-absolute img-bg-top-register"
      style={{ maxWidth: "100%", height: "500px" }}
    >
      <div className="container">
        <br />
        <br />
        <br />
        <img src={person} className="w-50 absolute mt-5 pt-5 " />
        <div className="card w-50  shadow border-0 container float-end">
          <div className="">
            <form onSubmit={handleSubmit}>
              <h5 className="mt-3 ms-5 text-right text-bold">Sign Up to Trash Track</h5>
              <center>
                <div class="mb-3 mt-3 content-center">
                  <input
                    type="text"
                    placeholder="username"
                    autocomplete="off"
                    className="form-control w-75 font-14"
                    id="exampleInputText1"
                    onChange={(e) => setUsername(e.target.value)}
                  />
                </div>

                <div class="mb-3 mt-3 content-center">
                  <input
                    type="text"
                    placeholder="Phone"
                    autocomplete="off"
                    className="form-control w-75 font-14"
                    id="exampleInputText1"
                    onChange={(e) => setNomorHp(e.target.value)}
                  />
                </div>
                <div class="mb-3 mt-3 content-center">
                  <input
                    type="email"
                    placeholder="Email"
                    autocomplete="off"
                    className="form-control w-75 font-14"
                    id="exampleInputText1"
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </div>
                <div class="mb-3 mt-3 content-center">
                  <input
                    type="password"
                    placeholder="Password"
                    autocomplete="off"
                    className="form-control w-75 font-14"
                    id="exampleInputText1"
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </div>

                <div className="mb-3">
                  <input
                    type="password"
                    placeholder="Re Enter Password"
                    autocomplete="off"
                    className="form-control w-75 font-14"
                    id="exampleInputText1"
                    onChange={(e) => setPasswordConfirm(e.target.value)}
                  />
                </div>
              </center>
              {/* <label>
                <p>Username</p>
                <input
                  type="text"
                  className="font-bold text-primary"
                  onChange={(e) => setEmail(e.target.value)}
                />
              </label>
              <label>
                <p>Password</p>
                <input
                  type="password"
                  onChange={(e) => setPassword(e.target.value)}
                />
              </label> */}
              <center>
                <div className="">
                  <Button
                    as="input"
                    type="submit"
                    variant="primary"
                    className="w-75 mt-2 mb-5"
                    value="Sign Up"
                  />
                </div>
                <img src={logo} className="" width="150" />
              </center>
              <br />
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
