import { useEffect, useState } from "react";
import {
  Badge,
  Button,
  Card,
  Navbar,
  Nav,
  Table,
  Container,
  Row,
  Col,
  Form,
  OverlayTrigger,
  Tooltip,
  Carousel,
} from "react-bootstrap";
import Modal from "react-bootstrap/Modal";
import add from "../../assets/img/add.svg";
import close from "../../assets/img/close-icon.svg";
import dump from "../../assets/img/dump.png";
import eye from "../../assets/img/eye-icon.svg";
import edit from "../../assets/img/edit-icon.svg";
import { dataUser, insertUser, userDeleteApi } from "../../api/Api";
import { toast, ToastContainer } from "react-toastify";
import UserTunggu from "./UserTunggu";
import UserProses from "./UserProses";
import { GoogleMap, Marker, MarkerF, useLoadScript } from "@react-google-maps/api";
import UserSelesai from "./UserSelesai";

function UserUpload() {
   // latitude
  const [latitude, setLatitude] = useState()
  const [longitude, setLongitude] = useState()
  const [show, setShow] = useState(true);
  const [state, setState] = useState([]); // data api user
  const [username, setUsername] = useState();
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();

  const handleShow = () => setShow(true);
  const handleClose = () => setShow(false);

  useEffect(() => {
    const fetchApi = async () => {
      const dataus = await dataUser();

      // slicing
      // const sliceData = dataus.slice(0, 6);

      // set long lat from current location
      navigator.geolocation.getCurrentPosition(
        function(position) {
          console.log(position.coords.latitude);
          setLatitude(position.coords.latitude);
          setLongitude(position.coords.longitude);
        },
        function(error) {
          console.error("Error Code = " + error.code + " - " + error.message);
        }
      );

      setState(dataus);
    };

    fetchApi();
  },5000);

  const submitForm = () => {
    const datares = insertUser({
      username,
      email,
      password,
    });
    toast("Berhasil di submit!", {
      position: toast.POSITION.BOTTOM_RIGHT,
      className: "foo-bar",
    });
    dataUser();
    handleClose();
    window.location.reload();
  };

  const deleteUser = (id) => {
    if (window.confirm("delete user?")) {
      const res = userDeleteApi(id);
      toast.success("delete success");
      console.log(res);
      // dataUser();
      handleClose();
      window.location.reload();
    }
  };


  const [userLat, setUserLat] = useState([
    {
      id: 1,
      lat: -6.654560,
      lng: 106.847070
    },
    {
      id: 2,
      lat: -6.628007,
      lng: 106.824478
    }
  ])

  const { isLoaded } = useLoadScript({
    googleMapsApiKey: process.env.REACT_APP_MAPS_API_KEY
  })

  if(!isLoaded) {
    return <div>Loading</div>
  }

  function Map() {
    return <GoogleMap     onClick={ev => {
      console.log("latitide = ", ev.latLng.lat());
      console.log("longitude = ", ev.latLng.lng());
    }} center={{ lat: latitude, lng: longitude}} zoom={14} mapContainerClassName="map-container">

      {userLat.map((value,index) => {
        return <MarkerF  position={{ lat: value.lat, lng: value.lng }} />
      })}
    </GoogleMap>
  }


  return (
    <div className="bg-white">
      <Container fluid>
        <span className="fs-5 fw-bold">Data User</span>
        <button
          type="button"
          onClick={handleShow}
          className="btn btn-primary rounded-pill mt-1  float-end"
          style={{ hover: "none" }}
        >
          <img src={add} className="mb-1" />
          <span className="fs-7 fw-bold mx-2">Tambah</span>
        </button>{" "}
        <br />
        <span className="fs-6">Home / Data user</span>
        <center>
          <div className="grid riwayat w-100 ">
            <div className="content bg-white" style={{ maxWidth: "700px" }}>
              <ul
                className="nav nav-pills nav-fill mb-3"
                id="pills-tab"
                role="tablist"
              >
                <li className="nav-item" role="presentation">
                  <button
                    className="nav-link  py-3"
                    id="pills-usertunggu-tab"
                    data-bs-toggle="pill"
                    data-bs-target="#pills-usertunggu"
                    type="button"
                    role="tab"
                    aria-controls="pills-usertunggu"
                    aria-selected="true"
                  >
                    Tunggu
                  </button>
                </li>
                <li className="nav-item" role="presentation">
                  <button
                    className="nav-link py-3"
                    id="pills-userproses-tab"
                    data-bs-toggle="pill"
                    data-bs-target="#pills-userproses"
                    type="button"
                    role="tab"
                    aria-controls="pills-userproses"
                    aria-selected="false"
                  >
                    Proses
                  </button>
                </li>
                <li className="nav-item" role="presentation">
                  <button
                    className="nav-link py-3"
                    id="pills-userselesai-tab"
                    data-bs-toggle="pill"
                    data-bs-target="#pills-userselesai"
                    type="button"
                    role="tab"
                    aria-controls="pills-userselesai"
                    aria-selected="false"
                  >
                    Selesai
                  </button>
                </li>
              </ul>
            </div>
          </div>
        </center>
        <div className="card rounded bg-primary mt-1">
          <div className=" w-100 text-white mt-2 mb-2">
            <span className="mb-2 fs-6 ms-3 fw-bold mt-2">
              Data berhasil ditambahkan
            </span>
            <img src={close} className="mx-3 mt-1 float-end" width="20" />
          </div>
        </div>
      </Container>

      <Modal size="lg" show={show} onHide={handleClose}>
        <Modal.Header closeButton className="bg-primary text-center">
          <center>
            <span className="fw-bold fs-6 text-white">Tambah Sampah</span>
          </center>
        </Modal.Header>

        <Modal.Body className="bg-primary text-white">
          <Form>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Nama Pengirim</Form.Label>
              <Form.Control
                type="text"
                placeholder="Nama Pengirim"
                onChange={(e) => setUsername(e.target.value)}
                autoFocus
              />
            </Form.Group>

            <Map />

            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Lokasi Sampah</Form.Label>
              <Form.Control
                type="file"
                placeholder=""
                onChange={(e) => setEmail(e.target.value)}
                autoFocus
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Tanggal</Form.Label>
              <Form.Control
                type="date"
                placeholder=""
                onChange={(e) => setEmail(e.target.value)}
                autoFocus
              />
            </Form.Group>
            <span className="">Jenis Sampah</span>
            <br/>
            <input
            className="form-check-input  "
            type="checkbox"
            // onClick={(e) => selectAll(e)}
            value=""
            id="flexCheckDefault"
          />
          <span className=" mx-3 ">Organik</span>
          <input
            className="form-check-input mx-4  "
            type="checkbox"
            // onClick={(e) => selectAll(e)}
            value=""
            id="flexCheckDefault"
          />
           <span className="">Anorganik</span>
          <input
            className="form-check-input mx-4  "
            type="checkbox"
            // onClick={(e) => selectAll(e)}
            value=""
            id="flexCheckDefault"
          />
           <span className="">Rumah Tangga</span>

            <Form.Group className="mb-3 mt-2" controlId="exampleForm.ControlInput1">
              <Form.Label>Foto Sampah </Form.Label>
              <Form.Control
                type="file"
                placeholder="Foto Sampah"
                onChange={(e) => setPassword(e.target.value)}
                autoFocus
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer className="bg-primary">

          <Button variant="primary" className="btn-green w-100 rounded-pill text-white fw-bold" onClick={submitForm}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
      <div className="tab-content min-vh-100" id="pills-tabContent">
        <UserTunggu />
        <UserProses />
        <UserSelesai />
      </div>
    </div>
  );
}

export default UserUpload;
