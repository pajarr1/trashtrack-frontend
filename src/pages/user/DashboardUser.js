import React, { useState } from "react";
// react-bootstrap components
import {
  Badge,
  Button,
  Card,
  Navbar,
  Nav,
  Table,
  Container,
  Row,
  Col,
  Form,
  OverlayTrigger,
  Tooltip,
  Carousel,
} from "react-bootstrap";
import lokasi from "../../assets/img/location.png";
import {
  GoogleMap,
  Marker,
  MarkerF,
  useLoadScript,
} from "@react-google-maps/api";
import Alert from "react-bootstrap/Alert";
import trashicon from "../../assets/img/trash-icon.svg";
import carousel2 from "../../assets/img/banner-user.png";
import carousel3 from "../../assets/img/banner-user2.png";

function DashboarUser() {
  const [show, setShow] = useState(true);
  const [modalShow, setModalShow] = useState(true);
  const handleShow = () => setModalShow(true);
  const handleClose = () => setModalShow(false);

  const [zoom, setZoom] = useState(10);

  const detailSampah = (id) => {
    setModalShow(true);
    console.log(id);
    setZoom(15);
  };

  const [userLat, setUserLat] = useState([
    {
      id: 1,
      lat: -6.65456,
      lng: 106.84707,
    },
    {
      id: 2,
      lat: -6.628007,
      lng: 106.824478,
    },
  ]);

  // maps
  // apiKey

  const { isLoaded } = useLoadScript({
    googleMapsApiKey: process.env.REACT_APP_MAPS_API_KEY,
  });

  if (!isLoaded) {
    return <div>Loading</div>;
  }

  function Map() {
    return (
      <GoogleMap
        center={{ lat: -6.65456, lng: 106.84707 }}
        zoom={zoom}
        mapContainerClassName="map-container"
      >
        {userLat.map((value, index) => {
          return (
            <MarkerF
              onClick={() => detailSampah(value.id)}
              position={{ lat: value.lat, lng: value.lng }}
            />
          );
        })}
      </GoogleMap>
    );
  }

  return (
    <div>
      <div className="content">
        <Container fluid>
          {/* <Alert show={show} variant="success" className="w-100">
            <Alert.Heading>Selamat Datang Pak RT!</Alert.Heading>
            <p>
              "Inspirasi merupakan anugerah dari kerja keras dan fokus." - Helen
              Hanson
            </p>
            <hr />
            <div className="d-flex justify-content-end">
              <Button onClick={() => setShow(false)} variant="outline-success">
                Hide
              </Button>
            </div>
          </Alert>

          {!show && (
            <Button onClick={() => setShow(true)} className="mb-3">
              Hi, Selamat datang di dashboard{" "}
            </Button>
          )} */}

          <Card className="border-0 shadow mt-3">
            <Row>
              <Col>
                <Carousel className="w-100 ">
                  <Carousel.Item interval={2000}>
                    <img
                      className="d-block w-100"
                      src={carousel2}
                      // alt="Second slide"
                    />
                  </Carousel.Item>
                  <Carousel.Item>
                    <img
                      className="d-block w-100"
                      src={carousel3}
                      // alt="Third slide"
                    />
                  </Carousel.Item>
                </Carousel>
              </Col>

              <Col lg="3" sm="6">
                <Card className="card-stats bg-primary border-0 shadow bg-warning me-4 mt-5">
                  <Card.Body>
                    <Row>
                      <Col xs="5">
                        <div className="icon-big text-center icon-blue">
                          <img src={trashicon} />
                        </div>
                      </Col>
                      <Col xs="7">
                        <div className="numbers">
                          <h6 className=" text-white">
                            Anda telah menambahkan
                          </h6>
                          <Card.Title as="h4" className="fw-bold text-primary ">
                            150
                          </Card.Title>
                          <h6 className=" text-white">Sampah</h6>
                        </div>
                      </Col>
                    </Row>
                  </Card.Body>
                  <Card.Footer className="bg-primary">
                    <hr></hr>
                  </Card.Footer>
                </Card>
                <Card className="card-stats bg-primary border-0 shadow bg-warning me-4 mt-5">
                  <Card.Body>
                    <Row>
                      <Col xs="5">
                        <div className="icon-big text-center icon-blue">
                          <img src={trashicon} />
                        </div>
                      </Col>
                      <Col xs="7">
                        <div className="numbers">
                          <h6 className=" text-white">
                            Anda telah menambahkan
                          </h6>
                          <Card.Title as="h4" className="fw-bold text-primary ">
                            150
                          </Card.Title>
                          <h6 className=" text-white">Sampah</h6>
                        </div>
                      </Col>
                    </Row>
                  </Card.Body>
                  <Card.Footer className="bg-primary">
                    <hr></hr>
                  </Card.Footer>
                </Card>
              </Col>
            </Row>
          </Card>

          <h5 className="fw-bold">Peta Sampah</h5>

          <Map style={{ maxHeight: "300px" }} />
        </Container>
      </div>
    </div>
  );
}

export default DashboarUser;
