import { useEffect, useState } from "react";
import {
  Badge,
  Button,
  Card,
  Navbar,
  Nav,
  Table,
  Container,
  Row,
  Col,
  Form,
  OverlayTrigger,
  Tooltip,
  Carousel,
} from "react-bootstrap";
import Modal from "react-bootstrap/Modal";
import add from "../../assets/img/add.svg";
import close from "../../assets/img/close-icon.svg";
import dump from "../../assets/img/dump.png";
import eye from "../../assets/img/eye-icon.svg";
import edit from "../../assets/img/edit-icon.svg";
import { dataUser, insertUser, userDeleteApi } from "../../api/Api";
import { toast, ToastContainer } from "react-toastify";

const UserSelesai = () => {
  //   const { api , setSpinner} = useContext(AppContext);
//   const [data, setData] = useState([]);
//   const history = useHistory();
//   const isFirstRun = useRef(true);

  //   useEffect(() => {
  //     // if (isFirstRun.current) {
  //     //   isFirstRun.current = false;
  //     //   return;
  //     // }
  //     getData();
  //   }, []);

  //   const getData = async () => {
  //     let param = {
  //       source: "Nara Store",
  //       status: "Dibatalkan",
  //       source: "Nara Store"
  //     };
  //     setSpinner(true);
  //     let data = await api("get", "transactions-grosir/get-by-user", param);
  //     setData(data.data);
  //     setSpinner(false);
  //   };

  return (
    <div
      className="tab-pane fade show "
      id="pills-userselesai"
      role="tabpanel"
      aria-labelledby="pills-userselesai-tab"
      tabIndex="0"
    >
      <Container fluid>
        <Row>
          <Col md={3}>
            {" "}
            <Card
              className="shadow border-0 m-2 cursor-pointer bg-primary text-white"
              style={{ maxWidth: "250px", height: "190px" }}
            >
              <Card.Title className="mt-2 ms-3">
                <span className="fw-bold fs-6 text-white ">07/02/2023</span>
              </Card.Title>
              <center>
                <Card.Img
                  className="w-100 item-center mt-3"
                  src={dump}
                  style={{ minHeight: "90px", minWidth: "210px" }}
                />
              </center>
              <Card.Body className="">
                <span className="fs-6 fw-bold">
                  <img src={edit} />
                </span>{" "}
                <span className="fs-6 fw-bold ms-5">Selesai</span>{" "}
                <span className="fs-6 fw-bold float-end">
                  <img src={eye} />
                </span>
              </Card.Body>
            </Card>
          </Col>
          <Col md={3}>
            {" "}
            <Card
              className="shadow border-0 m-2 cursor-pointer bg-primary text-white"
              style={{ maxWidth: "250px", height: "190px" }}
            >
              <Card.Title className="mt-2 ms-3">
                <span className="fw-bold fs-6 text-white ">07/02/2023</span>
              </Card.Title>
              <center>
                <Card.Img
                  className="w-100 item-center mt-3"
                  src={dump}
                  style={{ minHeight: "90px", minWidth: "210px" }}
                />
              </center>
              <Card.Body className="">
                <span className="fs-6 fw-bold">
                  <img src={edit} />
                </span>{" "}
                <span className="fs-6 fw-bold ms-5">Selesai</span>{" "}
                <span className="fs-6 fw-bold float-end">
                  <img src={eye} />
                </span>
              </Card.Body>
            </Card>
          </Col>
          <Col md={3}>
            {" "}
            <Card
              className="shadow border-0 m-2 cursor-pointer bg-primary text-white"
              style={{ maxWidth: "250px", height: "190px" }}
            >
              <Card.Title className="mt-2 ms-3">
                <span className="fw-bold fs-6 text-white ">07/02/2023</span>
              </Card.Title>
              <center>
                <Card.Img
                  className="w-100 item-center mt-3"
                  src={dump}
                  style={{ minHeight: "90px", minWidth: "210px" }}
                />
              </center>
              <Card.Body className="">
                <span className="fs-6 fw-bold">
                  <img src={edit} />
                </span>{" "}
                <span className="fs-6 fw-bold ms-5">Selesai</span>{" "}
                <span className="fs-6 fw-bold float-end">
                  <img src={eye} />
                </span>
              </Card.Body>
            </Card>
          </Col>
          <Col md={3}>
            {" "}
            <Card
              className="shadow border-0 m-2 cursor-pointer bg-primary text-white"
              style={{ maxWidth: "250px", height: "190px" }}
            >
              <Card.Title className="mt-2 ms-3">
                <span className="fw-bold fs-6 text-white ">07/02/2023</span>
              </Card.Title>
              <center>
                <Card.Img
                  className="w-100 item-center mt-3"
                  src={dump}
                  style={{ minHeight: "90px", minWidth: "210px" }}
                />
              </center>
              <Card.Body className="">
                <span className="fs-6 fw-bold">
                  <img src={edit} />
                </span>{" "}
                <span className="fs-6 fw-bold ms-5">Selesai</span>{" "}
                <span className="fs-6 fw-bold float-end">
                  <img src={eye} />
                </span>
              </Card.Body>
            </Card>
          </Col>
          <Col md={3}>
            {" "}
            <Card
              className="shadow border-0 m-2 cursor-pointer bg-primary text-white"
              style={{ maxWidth: "250px", height: "190px" }}
            >
              <Card.Title className="mt-2 ms-3">
                <span className="fw-bold fs-6 text-white ">07/02/2023</span>
              </Card.Title>
              <center>
                <Card.Img
                  className="w-100 item-center mt-3"
                  src={dump}
                  style={{ minHeight: "90px", minWidth: "210px" }}
                />
              </center>
              <Card.Body className="">
                <span className="fs-6 fw-bold">
                  <img src={edit} />
                </span>{" "}
                <span className="fs-6 fw-bold ms-5">Selesai</span>{" "}
                <span className="fs-6 fw-bold float-end">
                  <img src={eye} />
                </span>
              </Card.Body>
            </Card>
          </Col>
          <Col md={3}>
            {" "}
            <Card
              className="shadow border-0 m-2 cursor-pointer bg-primary text-white"
              style={{ maxWidth: "250px", height: "190px" }}
            >
              <Card.Title className="mt-2 ms-3">
                <span className="fw-bold fs-6 text-white ">07/02/2023</span>
              </Card.Title>
              <center>
                <Card.Img
                  className="w-100 item-center mt-3"
                  src={dump}
                  style={{ minHeight: "90px", minWidth: "210px" }}
                />
              </center>
              <Card.Body className="">
                <span className="fs-6 fw-bold">
                  <img src={edit} />
                </span>{" "}
                <span className="fs-6 fw-bold ms-5">Selesai</span>{" "}
                <span className="fs-6 fw-bold float-end">
                  <img src={eye} />
                </span>
              </Card.Body>
            </Card>
          </Col>
          <Col md={3}>
            {" "}
            <Card
              className="shadow border-0 m-2 cursor-pointer bg-primary text-white"
              style={{ maxWidth: "250px", height: "190px" }}
            >
              <Card.Title className="mt-2 ms-3">
                <span className="fw-bold fs-6 text-white ">07/02/2023</span>
              </Card.Title>
              <center>
                <Card.Img
                  className="w-100 item-center mt-3"
                  src={dump}
                  style={{ minHeight: "90px", minWidth: "210px" }}
                />
              </center>
              <Card.Body className="">
                <span className="fs-6 fw-bold">
                  <img src={edit} />
                </span>{" "}
                <span className="fs-6 fw-bold ms-5">Selesai</span>{" "}
                <span className="fs-6 fw-bold float-end">
                  <img src={eye} />
                </span>
              </Card.Body>
            </Card>
          </Col>
          <Col md={3}>
            {" "}
            <Card
              className="shadow border-0 m-2 cursor-pointer bg-primary text-white"
              style={{ maxWidth: "250px", height: "190px" }}
            >
              <Card.Title className="mt-2 ms-3">
                <span className="fw-bold fs-6 text-white ">07/02/2023</span>
              </Card.Title>
              <center>
                <Card.Img
                  className="w-100 item-center mt-3"
                  src={dump}
                  style={{ minHeight: "90px", minWidth: "210px" }}
                />
              </center>
              <Card.Body className="">
                <span className="fs-6 fw-bold">
                  <img src={edit} />
                </span>{" "}
                <span className="fs-6 fw-bold ms-5">Selesai</span>{" "}
                <span className="fs-6 fw-bold float-end">
                  <img src={eye} />
                </span>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </div>
  );
};
export default UserSelesai;
